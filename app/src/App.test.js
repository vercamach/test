import React from "react";
import { render, fireEvent, getByLabelText } from "@testing-library/react";
import App from "./App";
import Converter from "./components/converter";

// test("renders learn react link", () => {
//   const { getByText } = render(<App />);
//   const linkElement = getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

test("renders button", () => {
  const { getByText } = render(<Converter />);
  const button = getByText("Convert", { selector: "button" });
  expect(button).toBeInTheDocument();
});

test("get roman number in result", () => {
  const { getByText } = render(<Converter />);
  const input = getByLabelText("Latin number");
  fireEvent.change(input, { target: { value: 10 } });
  expect(input.value).toBe(10);
});
