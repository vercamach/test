import React, { useState } from "react";

export default function Converter() {
  const numbers = {
    1: "I",
    5: "V",
    10: "X",
    50: "L",
    100: "C",
    500: "D",
    1000: "M"
  };

  const [result, setResult] = useState(null);
  const [decimal, setDecimal] = useState(0);

  function onInputChange(e) {
    setDecimal(e.target.value);
  }

  function onButtonClick() {
    setResult(numbers[decimal]);
  }

  return (
    <div>
      <label htmlFor="numberInput">
        Latin number
        <input name="numberInput" type="number" onChange={onInputChange} />
      </label>
      <button onClick={onButtonClick}>Convert</button>
      <div>result: {result ? result : "add latin number to donvert"}</div>
    </div>
  );
}
