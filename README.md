# Roman Number Converter

Your task will be to create a converter that can convert decimal numbers into roman numbers.  

You need to prove to me that you can convert the numbers from 1 to 1000 in to Roman numbers.  

So a test driven approach will be good way to prove you have done things correctly.  

You may read about Roman numbers here: https://en.wikipedia.org/wiki/Roman_numerals

In order to solve this task you should in 60 minutes:  

1. Create a fork of this repository and send me the link.  
2. Set up a local environment that allows you to develop code and do unit tests in either Node.js, React or Python.  
3. Start solving the converter problem - commit every 10 minutes and push after 60 minutes.  
4. Show me the finished result including tests that prove you converted correctly.  

That's it - happy programming.  

/Zuuvi  
